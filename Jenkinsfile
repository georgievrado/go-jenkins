pipeline {
    // Lets Jenkins use Docker for us later.
    agent any

    parameters {
        string(defaultValue: 'alpine3.10', description: '', name: 'imageTargetOS')
        string(defaultValue: '1.12.9', description: 'Golang target version', name: 'goVersion')
        string(defaultValue: 'bitbucket.org/georgievrado/go-jenkins', description: '', name: 'srcPath')
    }

    // If anything fails, the whole Pipeline stops.
    stages {
        stage('Build') {
            // The build step build the application with the same Go version
            // We also want to make sure that the application is built under the same OS as the final docker image
            agent {
                docker {
                    image "golang:${params.goVersion}-${params.imageTargetOS}"
                    args '-e GOCACHE=/tmp/go-cache'
                }
            }

            steps {
                // Create our project directory.
                sh 'cd ${GOPATH}/src'
                sh "mkdir -p \${GOPATH}/src/${params.srcPath}"

                // Copy all files in our Jenkins workspace to our project directory.
                sh "cp -r \${WORKSPACE}/* \${GOPATH}/src/${params.srcPath}"
                sh "cd \${GOPATH}/src/${params.srcPath}"
                // Build the app.
                sh "go build -v -o go-jenkins"

            }
        }

        stage('Test') {
            // The alpine image is quite barebones and lacks the GCC compiler
            // To run the unit tests we will use the same Go version as the resulting docker container
            // However we will use a container that has the tools necessary for this step
            agent {
                docker {
                    image "golang:${params.goVersion}"
                    args '-e GOCACHE=/tmp/go-cache'
                }
            }

            steps {
                // Create our project directory.
                sh 'cd ${GOPATH}/src'
                sh "mkdir -p \${GOPATH}/src/${params.srcPath}"

                // Copy all files in our Jenkins workspace to our project directory.
                sh "cp -r \${WORKSPACE}/* \${GOPATH}/src/${params.srcPath}"

                // Remove cached test results.
                sh 'go clean -cache'

                // Run Unit Tests.
                sh 'go test ./... -v -short'
            }
        }

        stage('Docker') {
            steps {
                script {
                    node {
                        def myImage = ''
                        // Building the image
                        // The docker file uses the build artifact (go-jenkins) in Build stage
                        // to add it as the entrypoint of our container
                        stage('Build docker image'){
                            myImage = docker.build("georgievrado/go-jenkins", "--build-arg BASE_IMAGE_TAG=${params.goVersion}-${params.imageTargetOS} .")
                            // Test the build image
                            // You can check if all artifacts and files are in the container image
                            myImage.inside{
                                sh 'go version'
                            }
                        }

                        // Create a container from the new image and use it for integration tests
                        // here is a quite simplistic example - we use curl to make sure that
                        // we have a running process exosing our rest api
                        stage('Test docker image') {
                            def container = myImage.run('-p 8081:8080')
                            sh 'curl localhost:8081/HelloServer'
                            container.stop()
                        }

                        stage('Publish docker image'){
                            // Here we assume that Jenkins has the credentials for our image registry stored
                            withDockerRegistry(credentialsId: 'gtmhubcto-docker-hub') {
                                // some block
                                //myImage.push()
                            }
                        }
                    }
                }
            }
        }
    }

    post {
        always {
            // Clean up our workspace.
            deleteDir()
        }
    }
}