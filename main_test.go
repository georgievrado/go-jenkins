package main

import (
	"testing"
	"net/http/httptest"
	"io/ioutil"
	"net/http"
	"fmt"
)

func TestHelloServer(t *testing.T) {
	testPhrase := "world"
	testPath := fmt.Sprintf("/%s", testPhrase)
	r, err := http.NewRequest("GET", testPath, nil)
	if err != nil {
		t.Fatal(err)
	}

	w := httptest.NewRecorder()

	HelloServer(w, r)

	resp := w.Result()
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		t.Fail()
	}

	if string(body) != fmt.Sprintf("Hello, %s!", testPhrase) {
		t.Fail()
	}

}