ARG BASE_IMAGE_TAG=1.12.9-alpine3.10

FROM golang:$BASE_IMAGE_TAG
ADD go-jenkins /go/bin/go-jenkins

ENV CGO_ENABLED=0
ENV SECURED=true

ENTRYPOINT /go/bin/go-jenkins

EXPOSE 8080